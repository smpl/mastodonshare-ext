function saveOptions(e)
{
	browser.runtime.sendMessage(
		{
			"key": "setAccount", 
			"value": document.querySelector(".mastodon-widget-input").value
		}
	).then(
		response => {
			window.close();
		}
	);
}

document.addEventListener("DOMContentLoaded", function() {
	document.removeEventListener("DOMContentLoaded", this);
	
	browser.runtime.sendMessage({"key": "getAccount"}).then( 
		response => {
			document.querySelector(".mastodon-widget-input").value = response;
		}
	);

	document.querySelector(".mastodon-widget-button").addEventListener("click", saveOptions);
});
