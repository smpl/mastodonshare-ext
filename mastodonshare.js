var data = {
	account: "",
	sharebase: ""
};

function SetupAccount(result)
{
	if(!result.account) {
		console.log("No account in storage.");
		return;
	}
	data.account = result.account;

	if(data.account.charAt(0) == '@') {
			data.account = data.account.substring(1);
	}
	
	var uriparts = data.account.split('@');
	
	if(uriparts.length < 2)
		return;
	data.sharebase = 'https://' + uriparts[1] + '/share?text=';
}

browser.storage.local.get("account").then(SetupAccount);

browser.contextMenus.create({
  id: "share-link",
  title: browser.i18n.getMessage("menuItemShareLink"),
  contexts: ["link"]
});

browser.contextMenus.create({
  id: "share-page",
  title: browser.i18n.getMessage("menuItemSharePage"),
  contexts: ["page"]
});

browser.contextMenus.create({
  id: "share-image",
  title: browser.i18n.getMessage("menuItemShareImage"),
  contexts: ["image"]
});

browser.contextMenus.create({
  id: "share-frame",
  title: browser.i18n.getMessage("menuItemShareFrame"),
  contexts: ["frame"]
});

browser.contextMenus.create({
  id: "share-selection",
  title: browser.i18n.getMessage("menuItemShareSelection"),
  contexts: ["selection"]
});

browser.contextMenus.onClicked.addListener((info, tab) => 
{
	var text;
	
	if(data.account === "") {
		console.log("Error: No account.");
		return;
	}
	
	switch (info.menuItemId) {
		case "share-link": {
			text = info.linkUrl;
		break;
		}
		case "share-page": {
			text = info.pageUrl
			break;
		}
		case "share-image": {
			text = info.srcUrl;
			break;
		}
		case "share-frame": {
			text = info.frameUrl;
			break;
		}
		case "share-selection": {
			text = info.selectionText;
			break;
		}
		default: {
			return;
		}
	}

	browser.tabs.update({
		"url": data.sharebase + encodeURIComponent(text)
	});
});

browser.runtime.onMessage.addListener( async (msg) => {
	if(msg.key === "getAccount") {
		browser.storage.local.get("account").then(SetupAccount);
		return data.account;
	}
	if(msg.key === "setAccount") {
		browser.storage.local.set({
			account: msg.value
		}).then( result => {
			data.account = msg.value;
		});
		return;
	}
});
